<?php
/**
 * SoundCloud Download All track from user
 *
 * @category  Download
 * @package   Downloader
 */
require_once 'Soundcloud.php';
class Downloader extends Soundcloud
{
	protected $client_id;
	protected $client_secret;
	protected $user;
	
	protected $base = '/';
	protected $name;
	protected $link = 'https://api.soundcloud.com/users/%s/playlists.json';
	protected $param = array('limit' => 50, 'linked_partitioning' => 1, 'offset'=> 50);
	protected $ready = true;
	public function __construct( $config = array() ) {
		if( isset($config['client_id']) )      $this->client_id = $config['client_id'];
		if( isset($config['client_secret']) )  $this->client_secret = $config['client_secret'];
		if( isset($config['user']) )           $this->user = $config['user'];
		if( isset($config['path']) )           $this->path = $config['path'];
		if( isset($config['limit']) )          $this->param['limit'] = $config['limit'];
		$this->link = sprintf($this->link, $this->user);
		if( $this->path !== DIRECTORY_SEPARATOR) {
			$this->mkdir( $this->path);
		}
		parent::__construct( $this->client_id , $this->client_secret);
	}
	public function loop() {
		while( $this->ready ) {
			$this->getPlayList();
		}	
	}
	protected function getPlayList() {
		if( $this->ready ) {
			$response = json_decode( $this->get($this->link, $this->param) );
			//echo $response->next_href; die();
			if( ! isset($response->next_href ) )  {
				$this->ready = false;
			}
			else {
				$this->parseParam($response->next_href);
			}
			if( isset($response->collection) )
			{
				$this->getCollection($response->collection);
			}
		}
	}
	protected function getCollection($items)
	{
		if( count($items) > 0 )
		{
			foreach( $items as $item )
			{
				if( $item->downloadable )
				{
					$this->save($item->permalink, $item->tracks);
				}
			}
		}
	}
	protected function save($dir, $tracks) {
		$directory = trim($this->path, DIRECTORY_SEPARATOR). DIRECTORY_SEPARATOR . $dir;
		$this->mkdir($directory);
		$defaultParams = array('oauth_token' => $this->getAccessToken());
		foreach( $tracks as $track )
		{
			$url = $this->_buildUrl(
				'tracks/' . $track->id . '/download',
				$defaultParams
			);
			$content = $this->getContent($url);
			$name = $directory .'/'. $track->title.'.mp3';
			file_put_contents( $name, $content);		
		}	
	}
	protected function getContent($url) {
		$content = file_get_contents($url);
		return $content;
	}
	protected function parseParam($url) {
		$parts = parse_url($url);
		parse_str($parts['query'], $this->param);
	}
	protected function mkdir($dir) {
		if( !file_exists($dir) || ! is_dir( $dir)  ) {
			@mkdir($dir, 0777, true);
		}
	}
}
